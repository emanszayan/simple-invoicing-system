from odoo import models, fields, api


class Customer(models.Model):
    _name = 'customer.data'
    _inherits = {'res.partner': 'partner_id'}

    partner_id = fields.Many2one('res.partner')
    invoice=fields.One2many('customer.invoice','customer')
    total_amount_due=fields.Integer(compute="sum_amount_due_customer",string=" Total Amount Due")
    intial_balance=fields.Integer(compute="sum_tax", string=" Intial_Balance")
    @api.depends('invoice')
    def sum_amount_due_customer(self):

        records=self.env['customer.invoice'].sudo().search([('partner_id','=',self.name)])
        for record in records:
            self.total_amount_due +=record.residual_signed
        return self.total_amount_due


    @api.depends('invoice')
    def sum_tax(self):

        records=self.env['customer.invoice'].sudo().search([('partner_id','=',self.name)])
        for record in records:
            self.intial_balance +=record.amount_tax
        return self.intial_balance
from django.contrib import admin
from .models import Customer,Invoice

# Register your models here.
class CustomCustomer(admin.ModelAdmin):
    fieldsets=(
        ['customer info',{'fields':['customer_name','customer_phone','customer_email']}],
        )
    list_display=('customer_name','customer_phone','customer_email')
    list_filter=['customer_name']

class InlineInvoic(admin.StackedInline):
		model=Customer
		extra=1

class CustomInvoice(admin.ModelAdmin):
    fieldsets=(
        ['Invoice info',{'fields':['customer','invoice_date']}],
        )
    list_display=('customer','invoice_date')
    list_filter=['customer']


admin.site.register(Customer,CustomCustomer)
admin.site.register(Invoice,CustomInvoice)

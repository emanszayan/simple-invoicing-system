from django.apps import AppConfig


class SimpleinvoiceConfig(AppConfig):
    name = 'simpleinvoice'

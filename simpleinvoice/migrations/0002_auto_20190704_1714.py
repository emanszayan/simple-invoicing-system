# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-07-04 17:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('simpleinvoice', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='customer_email',
            field=models.EmailField(max_length=254),
        ),
    ]

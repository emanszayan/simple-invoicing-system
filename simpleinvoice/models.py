from django.db import models
import xmlrpc.client
import datetime

# Create your models here.
class Customer (models.Model):
    customer_name=models.CharField(max_length=50)
    customer_phone=models.CharField(max_length=14)
    customer_email=models.EmailField()
    def __str__(self):
        return self.customer_name
    def update(self, *args, **kwargs):
        if "deleted" in args:
            print("update",self.customer_name)
        super().update(**kwargs)
    def save(self, *args, **kwargs):
        url="http://localhost:8069"
        db="simple_invoice"
        username="admin"
        password="admin"
        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        print(common.version())
        uid = common.authenticate(db, username, password, {})
        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        print(models.execute_kw(db, uid, password,
            'customer.data', 'check_access_rights',
            ['read'], {'raise_exception': False}))
        if self.id == None:
            id = models.execute_kw(db, uid, password, 'customer.data', 'create', [{
                'name': self.customer_name,'phone':self.customer_phone,'email':self.customer_email
            }])
            print(id)
            # id = models.execute_kw(db, uid, password, 'customer.data', 'create', [{
            #     'partner_id': self.customer,'date_invoice':self.invoice_date

            # }])
            # print(id)
        else:
            user=Customer.objects.get(id=self.id)
            print("name",type(user.customer_name),user.customer_name)
            use_id=models.execute_kw(db, uid, password,
            'customer.data', 'search',
            [[['name', '=', user.customer_name]]])
            print(use_id[0])
            print(self.customer_name)
            models.execute_kw(db, uid, password, 'customer.data', 'write', [[use_id[0]], {
                'name': self.customer_name,'phone' :self.customer_phone ,'email':self.customer_email
            }])

        # for key in kwargs:
        #     print("aaaaaaaaaa",kwargs[key]) 
        
        super().save(*args, **kwargs)  # Call the "real" save() method.

    # def update(self, *args, **kwargs):
    #     print("uuuuuuuu",self.student_name)
    #     super().update(*args, **kwargs) 
    def delete(self):
        url="http://localhost:8069"
        db="simple_invoice"
        username="admin"
        password="admin"
        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        print(common.version())
        uid = common.authenticate(db, username, password, {})
        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        user=Customer.objects.get(id=self.id)
        print("name",type(user.customer_name),user.customer_name)
        use_id=models.execute_kw(db, uid, password,
        'customer.data', 'search',
        [[['name', '=', user.customer_name]]])
        print(use_id[0])
        print(self.customer_name)
        models.execute_kw(db, uid, password, 'customer.data', 'unlink', [[use_id[0]]])
        super().delete()




class Invoice(models.Model):
    customer=models.ForeignKey(Customer)
    invoice_date=models.DateField(("Date"), default=datetime.date.today)

    def __str__(self):
        return self.customer_name



    def save(self, *args, **kwargs):
        url="http://localhost:8069"
        db="simple_invoice"
        username="admin"
        password="admin"
        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        print(common.version())
        uid = common.authenticate(db, username, password, {})
        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        print(models.execute_kw(db, uid, password,
            'customer.invoice', 'check_access_rights',
            ['read'], {'raise_exception': False}))
        if self.id == None:
            id = models.execute_kw(db, uid, password, 'customer.invoice', 'create', [{
                'partner_id': self.customer,'date_invoice':self.invoice_date

            }])
            print(id)
        else:
            invoic=Invoice.objects.get(id=self.id)
            print("name",type(invoic.customer),invoic.customer_name)
            use_id=models.execute_kw(db, uid, password,
            'customer.data', 'search',
            [[['partner_id', '=', invoic.customer]]])
            print(use_id[0])
            print(self.customer_name)
            models.execute_kw(db, uid, password, 'customer.invoice', 'write', [[use_id[0]], {
                'partner_id': self.customer
            }])

        # for key in kwargs:
        #     print("aaaaaaaaaa",kwargs[key]) 
        
        super().save(*args, **kwargs)  # Call the "real" save() method.

    # def update(self, *args, **kwargs):
    #     print("uuuuuuuu",self.student_name)
    #     super().update(*args, **kwargs) 
    def delete(self):
        url="http://localhost:8069"
        db="simple_invoice"
        username="admin"
        password="admin"
        common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
        print(common.version())
        uid = common.authenticate(db, username, password, {})
        models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        use_id=models.execute_kw(db, uid, password,
            'customer.invoice', 'search',
            [[['partner_id', '=', self.customer]]])
        
        models.execute_kw(db, uid, password, 'customer.invoice', 'unlink', [[use_id[0]]])
        super().delete()
 